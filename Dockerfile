FROM alpine:3.12

ENV HOME=/home/duplicity \
  SCHEDULE="@daily" \
  HEALTHCHECK_PORT=8080

RUN set -x \
  && apk add --no-cache \
  ca-certificates \
  duplicity \
  curl \
  lftp \
  openssh \
  openssl \
  py3-crypto \
  py3-paramiko \
  py3-setuptools \
  py3-requests \
  py3-requests-oauthlib \
  rsync \
  && update-ca-certificates 

RUN curl -L https://github.com/prodrigestivill/go-cron/releases/download/v0.0.9/go-cron-linux-amd64-static.gz | zcat > /usr/local/bin/go-cron \
  && chmod a+x /usr/local/bin/go-cron \
  && apk del ca-certificates


COPY Pipfile Pipfile.lock /

RUN set -x \
  # Install Python dependencies.
  && wget https://bootstrap.pypa.io/get-pip.py -O- | python3 - \
  && pip install pipenv \
  && pipenv install --system --deploy --python=$(which python3) \
  && python3 -m pip uninstall -y pip pipenv setuptools

RUN set -x \
  # Run as non-root user.
  && adduser -D -u 1896 duplicity \
  && mkdir -p /home/duplicity/.cache/duplicity \
  && mkdir -p /home/duplicity/.gnupg \
  && chmod -R go+rwx /home/duplicity/ \
  # Brief check that it works.
  && su - duplicity -c 'duplicity --version'

VOLUME ["/home/duplicity/.cache/duplicity", "/home/duplicity/.gnupg"]

ENTRYPOINT ["/bin/sh", "-c"]

CMD ["exec /usr/local/bin/go-cron -s \"$SCHEDULE\" -p \"$HEALTHCHECK_PORT\" -- /backup.sh"]

HEALTHCHECK --interval=5m --timeout=3s \
  CMD curl -f "http://localhost:$HEALTHCHECK_PORT/" || exit 1
